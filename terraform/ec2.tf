resource "aws_instance" "ec2-webserver" {
  ami             = var.ami
  instance_type   = var.type
  key_name        = aws_key_pair.keypair.key_name
  security_groups = [aws_security_group.sg_pub.id]
  subnet_id = aws_subnet.awslab-subnet-public.id
  
  tags = {
    "name" = "ec2 webserver awslab"
  }

}

resource "aws_instance" "ec2-database" {
  ami             = var.ami
  instance_type   = var.type
  key_name        = aws_key_pair.keypair.key_name
  security_groups = [aws_security_group.sg_priv.id]
  subnet_id = aws_subnet.awslab-subnet-private.id

  ebs_block_device{
    device_name = "/dev/sdf"
    volume_size = 100
    volume_type = "standard"
  }
  
  tags = {
    "name" = "ec2 database awslab"
  }

}
