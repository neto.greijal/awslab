resource "aws_key_pair" "keypair" {
  key_name   = "keypair"
  public_key = file("~/.ssh/id_rsa_aws.pub")
}