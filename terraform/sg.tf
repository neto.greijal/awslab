resource "aws_security_group" "sg_pub" {
  name = "sg_pub"
  vpc_id = aws_vpc.awslab-vpc.id

  dynamic "ingress" {
    for_each = var.ingress_pub
    content { 
      description = ingress.value["description"]
      from_port = ingress.key
      to_port = ingress.key
      protocol = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    } 
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    description = "Allow icmp"
  }

  dynamic "egress" { 
    for_each = var.egress_pub
    content {
      description = egress.value["description"]
      from_port = egress.key
      to_port = egress.key
      protocol = egress.value["protocol"]
      cidr_blocks = egress.value["cidr_blocks"]
    }
  }

  tags = { 
    Name = "Security Group awslab" 
  } 

}

resource "aws_security_group" "sg_priv" {
  name = "sg_priv"
  vpc_id = aws_vpc.awslab-vpc.id

  dynamic "ingress" {
    for_each = var.ingress_priv
    content { 
      description = ingress.value["description"]
      from_port = ingress.key
      to_port = ingress.key
      protocol = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    } 
  }

  dynamic "egress" { 
    for_each = var.egress_priv
    content {
      description = egress.value["description"]
      from_port = egress.key
      to_port = egress.key
      protocol = egress.value["protocol"]
      cidr_blocks = egress.value["cidr_blocks"]
    }
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    description = "Allow icmp"
  }

  tags = { 
    Name = "Security Group awslab" 
  } 

}