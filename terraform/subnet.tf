resource "aws_subnet" "awslab-subnet-public" {
  vpc_id                  = aws_vpc.awslab-vpc.id
  cidr_block              = cidrsubnet(aws_vpc.awslab-vpc.cidr_block, 8, 1)
  availability_zone       = data.aws_availability_zones.az.names[0]
  map_public_ip_on_launch = true
  
  tags = {
    "name" = "Subnet public awslab"
  }
}

resource "aws_subnet" "awslab-subnet-private" {
  vpc_id                  = aws_vpc.awslab-vpc.id
  cidr_block              = cidrsubnet(aws_vpc.awslab-vpc.cidr_block, 8, 2)
  availability_zone       = data.aws_availability_zones.az.names[0]
  map_public_ip_on_launch = false
  
  tags = {
    "name" = "Subnet private awslab"
  }
}