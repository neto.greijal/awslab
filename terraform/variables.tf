# iam
variable "access_key" {
  default = "greijal"
}


# vpc
variable "vpcCIDRblock" {
  default = "172.16.0.0/16"
}

variable "dnsHostNames" {
  default = true  
}


# az
variable "region" {
  default = "us-east-2"
}

variable "az_count" {
  default = "1"
}


# sg
variable ingress_pub { 
  type = map(object({description = string, protocol = string, cidr_blocks = list(string)}))
  default = { 
    22 = { description = "Inbound para SSH", protocol = "tcp", cidr_blocks = [ "0.0.0.0/0" ] } 
    80 = { description = "Inbound para HTTP", protocol = "tcp", cidr_blocks = [ "0.0.0.0/0" ] }
  } 
}

variable egress_pub { 
  type = map(object({description = string, protocol = string, cidr_blocks = list(string)}))
  default = { 
    0 = { description = "Outbound All Trafic", protocol = "-1", cidr_blocks = [ "0.0.0.0/0" ] }
  } 
}

variable ingress_priv { 
  type = map(object({description = string, protocol = string, cidr_blocks = list(string)}))
  default = { 
    22 = { description = "Inbound para SSH", protocol = "tcp", cidr_blocks = [ "172.16.1.0/24" ] } 
    3110 = { description = "Inbound 3110", protocol = "tcp", cidr_blocks = [ "172.16.1.0/24" ] }
  } 
}

variable egress_priv { 
  type = map(object({description = string, protocol = string, cidr_blocks = list(string)}))
  default = { 
    0 = { description = "Outbound All Trafic", protocol = "-1", cidr_blocks = [ "0.0.0.0/0" ] }
  } 
}


# ec2
variable "ami" {
  default = "ami-023c8dbf8268fb3ca" # Amazon Linux AMI 2018.03.0 (HVM)
}

variable "type" {
  default = "t2.micro"
}
