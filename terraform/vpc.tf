resource "aws_vpc" "awslab-vpc" {
  cidr_block           = var.vpcCIDRblock
  enable_dns_hostnames = var.dnsHostNames
  
  tags = {
    "name" = "vpc awslab"
  }
}